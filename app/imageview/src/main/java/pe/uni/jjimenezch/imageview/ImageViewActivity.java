package pe.uni.jjimenezch.imageview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

public class ImageViewActivity extends AppCompatActivity {

    ImageView Image_1;
    Button boton_1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        Image_1 = findViewById(R.id.Image_1);
        boton_1 = findViewById(R.id.boton_1);

        boton_1.setOnClickListener(view -> Image_1.setImageResource(R.drawable.skyrim));
    }
}