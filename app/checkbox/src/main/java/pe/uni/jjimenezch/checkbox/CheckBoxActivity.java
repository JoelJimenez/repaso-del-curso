package pe.uni.jjimenezch.checkbox;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.TextView;

public class CheckBoxActivity extends AppCompatActivity {

    TextView textview_1;
    CheckBox checkBox_1;
    CheckBox checkBox_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);

        textview_1 = findViewById(R.id.textview_1);
        checkBox_1 = findViewById(R.id.checkbox_1);
        checkBox_2 = findViewById(R.id.checkbox_2);


        checkBox_1.setOnClickListener(view -> {
            if (checkBox_1.isChecked()){
                textview_1.setText(R.string.texto1);
                checkBox_2.setChecked(false);
            }else{
                textview_1.setText(R.string.mensaje);
            }
        });

        checkBox_2.setOnClickListener(view -> {
            if (checkBox_2.isChecked()){
                textview_1.setText(R.string.texto2);
                checkBox_1.setChecked(false);
            }else{
                textview_1.setText(R.string.mensaje);
            }
        });

    }
}