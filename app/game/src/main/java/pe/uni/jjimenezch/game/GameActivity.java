package pe.uni.jjimenezch.game;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class GameActivity extends AppCompatActivity {
    EditText editText_1;
    TextView textView_3;
    TextView textView_4;
    TextView textView_5;
    Button button_2;

    Boolean twoDigits, threeDigits, fourDigits;

    Random r = new Random();
    int random;

    int remainAttempts = 10;

    ArrayList<Integer> guessesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        textView_3 = findViewById(R.id.textView_3);
        textView_4 = findViewById(R.id.textView_4);
        textView_5 = findViewById(R.id.textView_5);
        editText_1 = findViewById(R.id.editText_1);
        button_2 = findViewById(R.id.button_2);

        /* primero debemos agarrar el intent*/

        twoDigits = getIntent().getBooleanExtra("TWO", false);
        threeDigits = getIntent().getBooleanExtra("THREE", false);
        fourDigits = getIntent().getBooleanExtra("FOUR", false);

        if(twoDigits){
            random = 10 + r.nextInt(90);
        }

        if(threeDigits){
            random = 100 + r.nextInt(900);
        }

        if(fourDigits){
            random = 1000 + r.nextInt(9000);
        }

        button_2.setOnClickListener(view -> {
            String sGuess = editText_1.getText().toString();
            if (sGuess.equals("")) {
                Toast.makeText(GameActivity.this,R.string.mensaje1,Toast.LENGTH_LONG).show();
                return;

            }
            remainAttempts--;



            int iGuess = Integer.parseInt(sGuess);

            guessesList.add(iGuess);

            textView_3.setVisibility(View.VISIBLE);
            textView_4.setVisibility(View.VISIBLE);
            textView_5.setVisibility(View.VISIBLE);

            // Esto se utiliza para concatenar cadenas con caracteres
            Resources res =getResources();
            textView_3.setText(String.format(res.getString(R.string.mensaje2),sGuess)); // Tu ultimo intento fue
            textView_4.setText(String.format(res.getString(R.string.mensaje3),remainAttempts));


            if(random == iGuess){

                // para continuar o cerrar el juego
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle(R.string.mensaje8);
                builder.setCancelable(false);
                builder.setMessage(String.format(res.getString(R.string.mensaje9),random,10-remainAttempts,guessesList));
                builder.setPositiveButton("si", (dialogInterface, i) -> {
                    Intent intent = new Intent(GameActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                });
                builder.setNegativeButton("NO", (dialogInterface, i) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });

                builder.create().show();
            }

            // gana

            if(random < iGuess){
                textView_5.setText(R.string.mensaje4);
            }

            if(random > iGuess){
                textView_5.setText(R.string.mensaje5);
            }

            // pierde
            if(remainAttempts == 0){

                // para continuar o cerrar el juego
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle(R.string.mensaje8);
                builder.setCancelable(false);
                builder.setMessage(String.format(res.getString(R.string.mensaje10),random,guessesList));
                builder.setPositiveButton("si", (dialogInterface, i) -> {
                    Intent intent = new Intent(GameActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                });
                builder.setNegativeButton("NO", (dialogInterface, i) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });

                builder.create().show();



            }

            editText_1.setText("");

        });
    }
}