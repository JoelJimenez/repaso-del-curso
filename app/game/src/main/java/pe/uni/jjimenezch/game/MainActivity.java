package pe.uni.jjimenezch.game;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    RadioButton radioButton_1;
    RadioButton radioButton_2;
    RadioButton radioButton_3;

    Button button_1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButton_1 = findViewById(R.id.radioButton_1);
        radioButton_2 = findViewById(R.id.radioButton_2);
        radioButton_3 = findViewById(R.id.radioButton_3);

        button_1 = findViewById(R.id.button_1);

        button_1.setOnClickListener(v -> {
            if (!radioButton_1.isChecked() && !radioButton_2.isChecked() && !radioButton_3.isChecked()) {
                Snackbar.make(v,R.string.texto6, Snackbar.LENGTH_LONG).show();
                return;
            }

            Intent intent = new Intent(MainActivity.this,GameActivity.class);

            if (radioButton_1.isChecked()){
                intent.putExtra("TWO",true);
            }
            if (radioButton_2.isChecked()){
                intent.putExtra("THREE",true);
            }
            if (radioButton_3.isChecked()){
                intent.putExtra("FOUR",true);
            }
            startActivity(intent);
        });
    }
}