package pe.uni.jjimenezch.messageactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MessagesActivity extends AppCompatActivity {

    Button boton_1;
    Button boton_2;
    Button boton_3;
    LinearLayout linearLayout_1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);


        boton_1 = findViewById(R.id.boton_1);
        boton_2 = findViewById(R.id.boton_2);
        boton_3 = findViewById(R.id.boton_3);
        linearLayout_1 = findViewById(R.id.linearLayout_1);


        /* para el boton toast*/

        boton_1.setOnClickListener(view -> Toast.makeText(getApplicationContext(),R.string.text1,Toast.LENGTH_LONG).show());

        boton_2.setOnClickListener(view -> Snackbar.make(linearLayout_1, R.string.text2, Snackbar.LENGTH_INDEFINITE).setAction(R.string.text3, view1 -> {

        }).show());

        boton_3.setOnClickListener(view -> {
            AlertDialog.Builder alerDialog = new AlertDialog.Builder(this);
            alerDialog
                    .setTitle(R.string.titulo1)
                    .setMessage(R.string.mensaje1)
                    .setNegativeButton(R.string.Negacion1, (dialog, i) -> dialog.cancel())
                    .setPositiveButton(R.string.Afirmacion1, (dialog, i) -> {

                    }).show();
            alerDialog.create();
        });

    } 
}