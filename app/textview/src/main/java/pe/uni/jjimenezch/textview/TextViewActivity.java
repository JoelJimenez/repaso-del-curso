package pe.uni.jjimenezch.textview;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class TextViewActivity extends AppCompatActivity {

    TextView Texto1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view);

        Texto1 = findViewById(R.id.Texto1);


        Texto1.setOnClickListener(view -> {
            Texto1.setText(R.string.Texto1_en);
            Texto1.setBackgroundColor(Color.RED);
            Texto1.setTextColor(Color.YELLOW);
        });
    }
}