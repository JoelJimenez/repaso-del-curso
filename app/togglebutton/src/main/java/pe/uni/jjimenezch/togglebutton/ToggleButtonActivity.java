package pe.uni.jjimenezch.togglebutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ToggleButton;

public class ToggleButtonActivity extends AppCompatActivity {

    ToggleButton toggleButton_1;
    ImageView imageView_1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toggle_button);

        toggleButton_1 = findViewById(R.id.toggleButton_1);
        imageView_1 = findViewById(R.id.imageView_1);

        toggleButton_1.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if(isChecked){
                imageView_1.setVisibility(View.INVISIBLE);
            }else{
                imageView_1.setVisibility(View.VISIBLE);
            }

        });


    }
}