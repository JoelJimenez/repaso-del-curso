package pe.uni.jjimenezch.gridview2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class GridView2Activity extends AppCompatActivity {


    GridView gridview;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view2);

        GridAdapter gridAdapter = new GridAdapter(GridView2Activity.this,text,image);
        gridview = findViewById(R.id.grid_view);
        fillArray();

        gridview.setAdapter(gridAdapter);


        gridview.setOnItemClickListener((parent, view, position, id) ->
                Toast.makeText(getApplicationContext(), text.get(position), Toast.LENGTH_LONG).show());
    }
    private void fillArray() {
        text.add("Primero");
        text.add("Segundo");
        text.add("Tercero");
        text.add("Cuarto");
        text.add("Quinto");
        text.add("Sexto");
        text.add("Septimo");
        text.add("Octavo");
        text.add("Noveno");
        image.add(R.drawable.a);
        image.add(R.drawable.b);
        image.add(R.drawable.c);
        image.add(R.drawable.d);
        image.add(R.drawable.e);
        image.add(R.drawable.f);
        image.add(R.drawable.g);
        image.add(R.drawable.h);
        image.add(R.drawable.p);
    }






}