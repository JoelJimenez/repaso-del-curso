package pe.uni.jjimenezch.button;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ButtonActivity extends AppCompatActivity {

    Button boton1;
    Button boton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);

        boton1 = findViewById(R.id.boton1);
        boton2 = findViewById(R.id.boton2);

        boton1.setOnClickListener(view -> {

            boton1.setText(R.string.texto1);
            boton1.setBackgroundColor(Color.RED);

            boton2.setVisibility(View.VISIBLE);

        });
    }
}