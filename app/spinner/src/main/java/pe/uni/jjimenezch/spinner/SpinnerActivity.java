package pe.uni.jjimenezch.spinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class SpinnerActivity extends AppCompatActivity {

    ImageView imageView_1;
    Spinner spinner_1;
    TextView textView_1;

    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        imageView_1 = findViewById(R.id.imageView_1);
        spinner_1 = findViewById(R.id.spinner_1);

        adapter = ArrayAdapter.createFromResource(this, R.array.Images, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner_1.setAdapter(adapter);
        spinner_1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){
                    imageView_1.setImageResource(R.drawable.a);
                }
                if(i == 1){
                    imageView_1.setImageResource(R.drawable.b);
                }
                if(i == 2){
                    imageView_1.setImageResource(R.drawable.c);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
}