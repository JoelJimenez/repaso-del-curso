package pe.uni.jjimenezch.radiobutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class RadioButtonActivity extends AppCompatActivity {

    TextView textView_1;
    ImageView imageView_1;
    RadioGroup radioGroup_1;
    RadioButton radioButton_1;
    RadioButton radioButton_2;
    RadioButton radioButton_3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_button);

        textView_1 = findViewById(R.id.textView_1);
        imageView_1 = findViewById(R.id.imageView_1);
        radioGroup_1 = findViewById(R.id.radioGroup_1);
        radioButton_1 = findViewById(R.id.radioButton_1);
        radioButton_2 = findViewById(R.id.radioButton_2);
        radioButton_3 = findViewById(R.id.radioButton_3);


        radioButton_1.setOnClickListener(view -> imageView_1.setImageResource(R.drawable.a));

        radioButton_2.setOnClickListener(view -> imageView_1.setImageResource(R.drawable.b));

        radioButton_3.setOnClickListener(view -> imageView_1.setImageResource(R.drawable.c));

    }
}