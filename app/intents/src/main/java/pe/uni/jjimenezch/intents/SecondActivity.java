package pe.uni.jjimenezch.intents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView textView_1;
    TextView textView_2;
    TextView textView_3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textView_1=findViewById(R.id.textView_1);
        textView_2=findViewById(R.id.textView_2);
        textView_3=findViewById(R.id.textView_3);

        Intent intent = getIntent();

        String text = intent.getStringExtra("Text");
        int number = intent.getIntExtra("Number",0);

        textView_1.setText(text);
        textView_2.setText(String.valueOf(number));

    }
}