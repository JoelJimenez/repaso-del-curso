package pe.uni.jjimenezch.intents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class IntentsActivity extends AppCompatActivity {

    Button boton_1;
    EditText editText_1;
    EditText editText_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intents);

        boton_1 = findViewById(R.id.boton_1);

        editText_1 = findViewById(R.id.editText_1);
        editText_2 = findViewById(R.id.editText_2);


        /* Donde sale y hacia donde va*/
        boton_1.setOnClickListener(view -> {

            String sText = editText_1.getText().toString();
            String sNumber = editText_2.getText().toString();

            Intent intent = new Intent(IntentsActivity.this, SecondActivity.class);

            intent.putExtra("Text", sText); /* inyectando info para poder trasladar mas adelante */

            if(!sNumber.equals("")){ /* Para revisar que haya alguna cadena*/
                int number = Integer.parseInt(sNumber);
                intent.putExtra("Number",number);
            }



            startActivity(intent);
            // finish(); /*se debe finalizar */


        });

    }
}