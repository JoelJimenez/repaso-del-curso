 package pe.uni.aliquel.gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

 public class GridViewActivity extends AppCompatActivity {
     GridView gridview;
     ArrayList<String> text = new ArrayList<>();
     ArrayList<Integer> image = new ArrayList<>();

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_grid_view);
         gridview = findViewById(R.id.grid_view);
         fillArray();

         GridAdapter gridAdapter = new GridAdapter(this, text, image);
         gridview.setAdapter(gridAdapter);

         gridview.setOnItemClickListener((parent, view, position, id) ->
                 Toast.makeText(getApplicationContext(), text.get(position), Toast.LENGTH_LONG).show());
     }
         private void fillArray() {
             text.add("hayasakaa");
             text.add("hayasakaai3");
             text.add("hayasakaai5");
             text.add("hayasakaai7");
             text.add("mehera");
             text.add("shinomiyakaguya10");
             text.add("shinomiyakaguya5");
             text.add("shiroganekei10");
             text.add("shiroganekei2c");
             image.add(R.drawable.hayasakaa);
             image.add(R.drawable.hayasakaai3);
             image.add(R.drawable.hayasakaai5);
             image.add(R.drawable.hayasakaai7);
             image.add(R.drawable.mehera);
             image.add(R.drawable.shinomiyakaguya10);
             image.add(R.drawable.shinomiyakaguya5);
             image.add(R.drawable.shiroganekei10);
             image.add(R.drawable.shiroganekei2c);
         }


 }