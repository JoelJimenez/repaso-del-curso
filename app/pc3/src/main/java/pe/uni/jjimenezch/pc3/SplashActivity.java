package pe.uni.jjimenezch.pc3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {

    TextView textView_1;
    ImageView imageView_1;
    Animation animationImage, animationText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        textView_1 = findViewById(R.id.textView_1);
        imageView_1=findViewById(R.id.imageView_1);


        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_animation);
        animationText = AnimationUtils.loadAnimation(this, R.anim.text_animation);

        imageView_1.setAnimation(animationImage);
        textView_1.setAnimation(animationText);

        /* aca en el count va intervalos de tiempo*/

        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this, miCalculadoraActivity.class);
                startActivity(intent);
                finish();
            }

        }.start();

    }
}