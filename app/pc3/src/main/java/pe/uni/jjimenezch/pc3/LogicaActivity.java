package pe.uni.jjimenezch.pc3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;

public class LogicaActivity extends AppCompatActivity {

    Button button_0;
    Button button_1;
    Button button_2;
    Button button_3;
    Button button_4;
    Button button_5;
    Button button_6;
    Button button_7;
    Button button_8;
    Button button_9;

    Button button_del;
    Button button_suma;
    Button button_igual;
    Button button_resta;
    Button button_division;
    Button button_producto;

    Button button_AC;

    TextView textView_Logica_1;

    float numero1 = 0.0f;
    float numero2 = 0.0f;

    String ope = "";

    float resultado;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logica);

        button_0 = findViewById(R.id.button_0);
        button_1 = findViewById(R.id.button_1);
        button_2 = findViewById(R.id.button_2);
        button_3 = findViewById(R.id.button_3);
        button_4 = findViewById(R.id.button_4);
        button_5 = findViewById(R.id.button_5);
        button_6 = findViewById(R.id.button_6);
        button_7 = findViewById(R.id.button_7);
        button_8 = findViewById(R.id.button_8);
        button_9 = findViewById(R.id.button_9);

        button_del = findViewById(R.id.button_del);
        button_suma = findViewById(R.id.button_suma);
        button_igual = findViewById(R.id.button_igual);
        button_resta = findViewById(R.id.button_resta);
        button_division = findViewById(R.id.button_division);
        button_producto = findViewById(R.id.button_producto);

        button_AC = findViewById(R.id.button_AC);

        textView_Logica_1 = findViewById(R.id.textView_Logica_1);
        textView_Logica_1.setText("0");

        button_0.setOnClickListener(view1 -> {
            float temp = Float.parseFloat(textView_Logica_1.getText().toString());
            if(temp == 0.0f){
                textView_Logica_1.setText("0");
            }
            else {
                textView_Logica_1.setText(textView_Logica_1.getText()+"0");
            }
        });

        button_1.setOnClickListener(view1 -> {
            float temp = Float.parseFloat(textView_Logica_1.getText().toString());
            if(temp == 0.0f){
                textView_Logica_1.setText("1");
            }
            else {
                textView_Logica_1.setText(textView_Logica_1.getText()+"1");
            }
        });

        button_2.setOnClickListener(view1 -> {
            float temp = Float.parseFloat(textView_Logica_1.getText().toString());
            if(temp == 0.0f){
                textView_Logica_1.setText("2");
            }
            else {
                textView_Logica_1.setText(textView_Logica_1.getText()+"2");
            }
        });

        button_3.setOnClickListener(view1 -> {
            float temp = Float.parseFloat(textView_Logica_1.getText().toString());
            if(temp == 0.0f){
                textView_Logica_1.setText("3");
            }
            else {
                textView_Logica_1.setText(textView_Logica_1.getText()+"3");
            }
        });


        button_4.setOnClickListener(view1 -> {
            float temp = Float.parseFloat(textView_Logica_1.getText().toString());
            if(temp == 0.0f){
                textView_Logica_1.setText("4");
            }
            else {
                textView_Logica_1.setText(textView_Logica_1.getText()+"4");
            }
        });

        button_5.setOnClickListener(view1 -> {
            float temp = Float.parseFloat(textView_Logica_1.getText().toString());
            if(temp == 0.0f){
                textView_Logica_1.setText("5");
            }
            else {
                textView_Logica_1.setText(textView_Logica_1.getText()+"5");
            }
        });

        button_6.setOnClickListener(view1 -> {
            float temp = Float.parseFloat(textView_Logica_1.getText().toString());
            if(temp == 0.0f){
                textView_Logica_1.setText("6");
            }
            else {
                textView_Logica_1.setText(textView_Logica_1.getText()+"6");
            }
        });

        button_7.setOnClickListener(view1 -> {
            float temp = Float.parseFloat(textView_Logica_1.getText().toString());
            if(temp == 0.0f){
                textView_Logica_1.setText("7");
            }
            else {
                textView_Logica_1.setText(textView_Logica_1.getText()+"7");
            }
        });

        button_8.setOnClickListener(view1 -> {
            float temp = Float.parseFloat(textView_Logica_1.getText().toString());
            if(temp == 0.0f){
                textView_Logica_1.setText("8");
            }
            else {
                textView_Logica_1.setText(textView_Logica_1.getText()+"8");
            }
        });

        button_9.setOnClickListener(view1 -> {
            float temp = Float.parseFloat(textView_Logica_1.getText().toString());
            if(temp == 0.0f){
                textView_Logica_1.setText("9");
            }
            else {
                textView_Logica_1.setText(textView_Logica_1.getText()+"9");
            }
        });

        button_AC.setOnClickListener(view1 -> {
            numero1 = Float.parseFloat(textView_Logica_1.getText().toString());
            textView_Logica_1.setText("0");
            numero1=0.0f;
            numero2=0.0f;
            ope = "";
        });


        button_division.setOnClickListener(view -> {
            numero1 = Float.parseFloat(textView_Logica_1.getText().toString());
            ope = "/";
            textView_Logica_1.setText("0");
        });

        button_suma.setOnClickListener(view -> {
            numero1 = Float.parseFloat(textView_Logica_1.getText().toString());
            ope = "+";
            textView_Logica_1.setText("0");
        });

        button_resta.setOnClickListener(view -> {
            numero1 = Float.parseFloat(textView_Logica_1.getText().toString());
            ope = "-";
            textView_Logica_1.setText("0");
        });

        button_producto.setOnClickListener(view -> {
            numero1 = Float.parseFloat(textView_Logica_1.getText().toString());
            ope = "*";
            textView_Logica_1.setText("0");
        });

        button_del.setOnClickListener(view -> {
            // numero1 = Float.parseFloat(textView_Logica_1.getText().toString());
            String cadena = textView_Logica_1.getText().toString();
            if(cadena.length()==1) {
                textView_Logica_1.setText("0");
            }else {
                textView_Logica_1.setText(cadena.substring(0, cadena.length() - 1));
            }
        });


        button_igual.setOnClickListener(view -> {
            numero2 = Float.parseFloat(textView_Logica_1.getText().toString());
            if(ope.equals("/")){
                if(numero2==0){
                    textView_Logica_1.setText("0");
                    Toast.makeText(LogicaActivity.this,"La operacion no es válida",Toast.LENGTH_LONG).show();
                }else{
                    resultado = numero1/numero2;
                    textView_Logica_1.setText(resultado+"");
                }
            }
            if(ope.equals("+")){
                resultado = numero1+numero2;
                textView_Logica_1.setText(resultado+"");
            }

            if(ope.equals("*")){
                resultado = numero1*numero2;
                textView_Logica_1.setText(resultado+"");
            }

            if(ope.equals("-")){
                resultado = numero1-numero2;
                textView_Logica_1.setText(resultado+"");
            }
        });

    }

}