package pe.uni.jjimenezch.pc3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.Toast;

public class miCalculadoraActivity extends AppCompatActivity {

    RadioButton radioButton_1;
    RadioButton radioButton_2;
    RadioButton radioButton_3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_calculadora);


        radioButton_1 = findViewById(R.id.radioButton_1);
        radioButton_2 = findViewById(R.id.radioButton_2);
        radioButton_3 = findViewById(R.id.radioButton_3);

        radioButton_1.setOnClickListener(view -> {
            // Para cambiar de pagina es necesario estas 2 sentecias
            Intent intent = new Intent(miCalculadoraActivity.this,LogicaActivity.class);
            startActivity(intent);
        });


        radioButton_2.setOnClickListener(view -> Toast.makeText(miCalculadoraActivity.this,R.string.opcionInvalida,Toast.LENGTH_LONG).show());

        radioButton_3.setOnClickListener(view -> Toast.makeText(miCalculadoraActivity.this,R.string.opcionInvalida,Toast.LENGTH_LONG).show());

        }
}