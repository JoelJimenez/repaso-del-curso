package pe.uni.jjimenezch.edittest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditTestActivity extends AppCompatActivity {

    EditText edit_text_one;
    Button boton_one;
    TextView text_view_one;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_test);

        edit_text_one = findViewById(R.id.edit_text_one);
        boton_one = findViewById(R.id.boton_one);
        text_view_one = findViewById(R.id.text_view_one);


        boton_one.setOnClickListener(view-> {
            String name = edit_text_one.getText().toString();
            text_view_one.setText(name);
        });

    }
}